<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>View</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
	<div class="alb">
		<?php 
		 include "../database/db.php";
		 $sql = "SELECT * FROM videos ORDER BY id ASC LIMIT 1";
		 $res = mysqli_query($conn, $sql);

		 if (mysqli_num_rows($res) > 0) {
		 	while ($video = mysqli_fetch_assoc($res)) { 
		 ?>
	        <video src="../uploads/<?=$video['video_url']?>" 
	        	   controls>
	        	
	        </video>
	    <?php 
	     }
		 }else {
		 	echo "<h1>Empty</h1>";
		 }
		 
		 
		 ?>
	</div>
</body>
</html>