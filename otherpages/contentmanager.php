<?php
session_start();

if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] == false) {
    header("location: contentmanager.php");
    exit;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Content Manager</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../css/style.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="../otherpages/contentmanager.html">
                <div class="sidebar-brand-text mx-3">StreamWithAldrin</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="index.html">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>ROLE: CONTENT MNGR. </span></a>
            </li>

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>


        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"> </i>
                        
                    </button>

                    
                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">


                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                
                                <img class="img-profile rounded-circle"
                                    src="../Attributes/img.jpg">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                            
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">WELCOME BACK!</h1>
                        <a href="#" class=""><i class=""></i></a>
                    </div>

                    <div class="row">

                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-uppercase mb-1">
                                                Upload Media</div>
                                           
                                            <form action="../php/deletefirstVideo.php" method="post">  
                                            <input class="imageup" type="submit" value="Remove">
                                            </form>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                        <div class="col-xl-6 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body" id="fileDetailsContainer">
                                     <form action="../php/uploadVideo.php" method="post"enctype="multipart/form-data">
                                            <input class="imageup"type="submit"name="submit"value="Upload">
                                            <input class="imageup" type="file"name="my_video"> 
                                            </form>
                                </div>
                            </div>
                        </div>
                    
                    </div>
                    
                    <div id="fileViewModal">
                        <div class="modal-content">
                            <span class="close-btn" onclick="closeFileViewModal()">&times;</span>
                            <h2>Uploaded Files</h2>
                            <ul id="fileList"></ul>
                        </div>
                    </div>

                    <!-- Content Row -->

                    <div class="row">


                        <!-- Area Chart -->
                        <div class="col-xl-8 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">• Currently Live</h6>
                                    
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                    <?php 
                                include "../database/db.php";
                                $sql = "SELECT * FROM videos ORDER BY id ASC LIMIT 1";
                                $res = mysqli_query($conn, $sql);

                                if (mysqli_num_rows($res) > 0) {
                                while ($video = mysqli_fetch_assoc($res)) { 
                                ?>
                                <video id="conqueuevid" src="../uploads/<?=$video['video_url']?>" controls></video>
                                <?php 
                                }
                                } else {
                                echo "<h1>Empty</h1>";
                                }
                                ?>
                                </div>
                            </div>
                        </div>

                        <!-- Pie Chart -->
                        <div class="col-xl-4 col-lg-5">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Queue</h6>
                                
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                <div class="alb" style="overflow: auto; max-height: 300px;">
                                <?php 
                                include "../database/db.php";
                                $sql = "SELECT * FROM videos ORDER BY id ASC";
                                $res = mysqli_query($conn, $sql);

                                if (mysqli_num_rows($res) > 0) {
                                while ($video = mysqli_fetch_assoc($res)) { 
                                ?>
                                <video id="queuevid" src="../uploads/<?=$video['video_url']?>" controls></video>
                                <?php 
                                }
                                } else {
                                echo "<h1>Empty</h1>";
                                }
                                ?>
                                </div>
                                 <div class="mt-4 text-center small">
                                 <span class="mr-2"></span>   
                                 </div>
                               </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Aldrei Fat Choi - 9474AB - IT312/312L - 1st Semester AY 2023 - 2024</span>
                        <span>IT Department</span> 
                        <span>School of Accountancy, Management, Computing and Information Studies</span>
                        <span>Saint Louis University</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="../Scripts/jquery.min.js"></script>
    <script src="../Scripts/bootstrap.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="../Scripts/scr.js"></script>
    <script src="../Scripts/contentmngr.js"></script>


</body>

</html>