-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 10, 2023 at 06:28 PM
-- Server version: 8.0.31
-- PHP Version: 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aldrinstream`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
CREATE TABLE IF NOT EXISTS `accounts` (
  `username` varchar(20) NOT NULL,
  `f_name` varchar(50) NOT NULL,
  `l_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(70) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `role` varchar(30) NOT NULL,
  `forgot_pass_status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`username`, `f_name`, `l_name`, `email`, `password`, `role`, `forgot_pass_status`) VALUES
('akosicarl', 'Carl Kendrick', 'Pascua', 'ckpascua@slu.edu.ph', '$2y$10$zyUDYRh.anow0eDDgjpIt.gDCOZWOdRvyTBqqRgTHP.gTyotV133i', 'Content Manager', 0),
('angela', 'Nelle Angela', 'Ramat', 'naramat@slu.edu.ph', '$2y$10$gYSFW0ulvw4GhgLGxDJk8.bj7xjRGTNHIk19QlkdWy11tB72V75XG', 'Admin', 0),
('chef', 'Stephen', 'Curry', 'sc30@slu.edu.ph', '$2y$10$FXy3mF9F.EKDYIhqtOy8ReMz6tZo.CzLXqewUtUGA5onhmuJyHx52', 'Content Manager', 0),
('eddy', 'Eddyson', 'Aromin', 'etaromin@slu.edu.ph', '$2y$10$JdI6ZLWhzRL/Rsoul0sDJufAZfsmORGrTLNqgRfYAOV7moxwH48ly', 'Content Manager', 0),
('king', 'Lebron', 'James', 'lbj@slu.edu.ph', '$2y$10$BB.cvbDo10Xt92jHNKUBNevortG5XURMpirjht4NcZFYZWK6ajIUq', 'Content Manager', 0),
('kyle', 'Gerald Kyle', 'Angway', 'gkangway@slu.edu.ph', '$2y$10$7lvoKXt7etGF/M1hXuAUteU2l97tH27hvDSaIM1O.3ckmSedbIYmS', 'Admin', 0),
('menna', 'Jekka Menna', 'Hufalar', 'jmhufalar@slu.edu.ph', '$2y$10$Wjq55sjjEfViZ0ywnL.4TuJh9lYRu9.Bhp2fihX3DUNV2tib2aV0S', 'Content Manager', 0),
('potato', 'Justin', 'Montemayor', 'jjmontemayor@slu.edu.ph', '$2y$10$6DNDNnqcqk3L5lBEUfgv.eGJlxZNxZqipFiGfFO.dFE3V6XFV6owy', 'Content Manager', 0);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
CREATE TABLE IF NOT EXISTS `media` (
  `media_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `file_type` int NOT NULL,
  `path` int NOT NULL,
  `size` int NOT NULL,
  `username` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `time_uploaded` time NOT NULL,
  PRIMARY KEY (`media_id`),
  KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `saved_show`
--

DROP TABLE IF EXISTS `saved_show`;
CREATE TABLE IF NOT EXISTS `saved_show` (
  `show_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `saved_path` varchar(100) NOT NULL,
  `saved_size` int NOT NULL,
  PRIMARY KEY (`show_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `media`
--
ALTER TABLE `media`
  ADD CONSTRAINT `media_ibfk_1` FOREIGN KEY (`username`) REFERENCES `accounts` (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
