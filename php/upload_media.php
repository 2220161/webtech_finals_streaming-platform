<?php
    include 'MySQLConnection.php';

    $serverName = "localhost";
    $u_name = "root";
    $p_word = "";
    $dbname = "aldrinstream";

    $conn = new mysqli ($serverName, $u_name, $p_word, $dbname);

    if(isset($_POST['upload-media'])){
        $username = $_POST['username'];
        $title = $_POST['title'];
        $file_type = $_POST['file_type'];
        $path = $_POST['path'];
        $size = $_POST['size'];
        $date = $_POST['date'];
        $time_uploaded = $_POST['time_uploaded'];

        if(size > 6656){
            echo "File cannot exceed more than 65KB";
        }else{
            $stmt = $conn->prepare("INSERT INTO media (username, title, file_type, path, size) VALUES (?,?,?,?,?)");
            $stmt->bind_param("ssssd", $username, $title, $file_type, $path, $size);
            $stmt->execute();
            $stmt->close();
            echo "New media successfully uploaded"; // Notification
        }
    }

?>
