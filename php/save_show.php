<?php
    include 'MySQLConnection.php';

    $serverName = "localhost";
    $u_name = "root";
    $p_word = "";
    $dbname = "aldrinstream";

    $conn = new mysqli ($serverName, $u_name, $p_word, $dbname);

    if(isset($_POST['save-show'])){
        $show_id = $_POST['show_id'];
        $title = $_POST['title'];
        $date = $_POST['date'];
        $saved_path = $_POST['saved_path'];
        $saved_size = $_POST['saved_size'];

        $stmt = $conn->prepare("INSERT INTO saved_show (show_id, title, date, saved_path, saved_size) VALUES (?,?,?,?,?)");
        $stmt->bind_param("ssssd", $show_id, $title, $date, $saved_path, $saved_size);
        $stmt->execute();
        $stmt->close();
        echo "New media successfully uploaded"; // Notification
        }
    }

?>
