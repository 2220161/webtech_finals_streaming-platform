<?php
    include 'MySQLConnection.php';

        $serverName = "localhost";
        $u_name = "root";
        $p_word = "";
        $dbname = "aldrinstream";

        $conn = new mysqli ($serverName, $u_name, $p_word, $dbname);

        if(isset($_POST['update_account'])){
            $username = $_POST['username'];
            $password = $_POST['password'];

            $stmt = $conn->prepare("UPDATE accounts SET password = ? WHERE username = ?");
            $stmt->bind_param("ss", $password, $username);
            $stmt->execute();
            $stmt->close();
            echo "Password is updated"; // Notification
        }