<?php
include '../database/db.php';

$sql = "DELETE FROM `videos` ORDER BY id ASC LIMIT 1";

if ($conn->query($sql) === TRUE) {
    echo "Record deleted successfully";
} else {
    echo "Error deleting record: " . $conn->error;
}

$conn->close();

header("Location: ../otherpages/contentmanager.php");
exit();
?>
