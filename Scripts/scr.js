function triggerFileInput() {
  document.getElementById('fileInput').click();
}

function displayFileDetails() {
  const fileInput = document.getElementById('fileInput');
  const fileDetailsContainer = document.getElementById('fileDetailsContainer');

  fileDetailsContainer.innerHTML = ''; // Clear the file details container

  for (const file of fileInput.files) {
      const detailsContainer = document.createElement('div');
      detailsContainer.classList.add('file-details');

      // Display file icon based on file type
      const fileIcon = document.createElement('i');
      fileIcon.classList.add('file-icon');
      if (file.type.startsWith('audio/')) {
          fileIcon.classList.add('fas', 'fa-music');
      } else if (file.type.startsWith('video/')) {
          fileIcon.classList.add('fas', 'fa-video');
      } else {
          fileIcon.classList.add('fas', 'fa-file');
      }
      detailsContainer.appendChild(fileIcon);

      // Display file details
      const fileNameParagraph = document.createElement('p');
      fileNameParagraph.textContent = `File Name: ${file.name}`;
      detailsContainer.appendChild(fileNameParagraph);

      const fileSizeParagraph = document.createElement('p');
      fileSizeParagraph.textContent = `File Size: ${formatBytes(file.size)}`;
      detailsContainer.appendChild(fileSizeParagraph);

      // View file button
      const viewFileButton = document.createElement('button');
      viewFileButton.textContent = 'View File';
      viewFileButton.classList.add('view-btn');
      viewFileButton.onclick = () => viewFile(file);
      detailsContainer.appendChild(viewFileButton);

      fileDetailsContainer.appendChild(detailsContainer);
  }
}

function openFileViewModal() {
  const modal = document.getElementById('fileViewModal');
  const fileListContainer = document.getElementById('fileList');

  fileListContainer.innerHTML = ''; // Clear the file list container

  // Populate the file list
  const fileInput = document.getElementById('fileInput');
  for (const file of fileInput.files) {
      const listItem = document.createElement('li');
      listItem.textContent = `${file.name} - ${formatBytes(file.size)}`;
      fileListContainer.appendChild(listItem);
  }

  modal.style.display = 'flex';
}

function closeFileViewModal() {
  const modal = document.getElementById('fileViewModal');
  modal.style.display = 'none';
}

function viewFile(file) {
  // Open a new tab to view the file
  window.open(URL.createObjectURL(file));
}

function formatBytes(bytes, decimals = 2) {
  if (bytes === 0) return '0 Bytes';

  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}