<?php
require('../database/db.php');
session_start();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $username = $_POST['username'];
    $email = $_POST['email'];

    $stmt = $db->prepare("SELECT * FROM accounts WHERE username = ?");
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();

        if ($row['role'] == 'Admin' || $row['role'] == 'admin') {
            echo "<script>alert('Admin cannot request to change password');</script>";
        } elseif ($row['email'] == $email) {
            if ($row['forgot_pass_status'] == 1) {
                echo "<script>
                                alert('Request already sent, please wait');
                                window.location.href = 'index.php';
                              </script>";
            } else {
                $updateStmt = $db->prepare("UPDATE accounts SET forgot_pass_status = 1 WHERE username = ?");
                $updateStmt->bind_param("s", $username);
                $updateStmt->execute();
                $updateStmt->close();

                echo "<script>alert('Request sent');</script>";
            }
        } else {
            echo "<p style='color: red;'>Invalid username or email</p>";
        }
    } else {
        echo "<p style='color: red;'>Account not found</p>";
    }
    $stmt->close();
    $db->close();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Forgot Password</title>
    <link rel="stylesheet" href="../css/style1.css">
</head>
<body>
    <h2>Forgot Password</h2>
    <form action="forgot_pass.php" method="post">
        <input type="text" id="username" name="username" placeholder="Username" required><br><br>
        <input type="email" id="email" name="email" placeholder="Email" required><br><br>
        <input type="submit" value="Submit">
    </form>

    <form action="index.php">
        <button type="submit">Back</button>
    </form>
</body>
</html>