<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login</title>
    <script src="login.js" defer></script>
    <link rel="stylesheet" href="../css/styles.css">
</head>
<body>

<h2>Login</h2>

<?php
    session_start();
    if(isset($_SESSION['error'])) {
        $error = $_SESSION['error'];
        echo "<p style='color: red;'>$error</p>";
        unset($_SESSION['error']);
    }
?>

<form action="login.php" method="post">
    <label for="username">Username:</label>
    <input type="text" id="username" name="username" required><br><br>
    <label for="password">Password:</label>
    <input type="password" id="password" name="password" required>
    <input type="checkbox" onclick="toggleLoginPassword()">Show Password<br><br>
    <input type="submit" value="Login">
    <a href="forgot_pass.php">Forgot Password?</a>
</form>
</body>
</html>