<?php
require('../database/db.php');
require('require_session.php');

$response = array('success' => false, 'message' => '');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $adminPassword = $_POST['adminPassword'];
    $adminUsername = $_SESSION['username'];

    $adminCheckSql = "SELECT password FROM accounts WHERE username = '$adminUsername' AND role = 'Admin'";
    $adminCheckResult = $db->query($adminCheckSql);

    if ($adminCheckResult->num_rows === 1) {
        $adminData = $adminCheckResult->fetch_assoc();
        $hashedAdminPassword = $adminData['password'];

        if (password_verify($adminPassword, $hashedAdminPassword)) {
            $newUsername = $_POST['newUsername'];
            $firstName = $_POST['firstName'];
            $lastName = $_POST['lastName'];
            $newEmail = $_POST['newEmail'];
            $newPassword = password_hash($_POST['newPassword'], PASSWORD_DEFAULT);
            $newRole = $_POST['newRole'];

            $sql = "INSERT INTO accounts (username, f_name, l_name, email, password, role)
                    VALUES ('$newUsername', '$firstName', '$lastName', '$newEmail', '$newPassword', '$newRole')";

            if ($db->query($sql)) {
                $response['success'] = true;
                $response['message'] = 'User added successfully!';
            } else {
                $response['message'] = 'Error: ' . $db->error;
            }
        } else {
            $response['message'] = 'Invalid admin password.';
        }
    }

    $adminCheckResult->free_result();
}

$db->close();

echo json_encode($response);
?>
