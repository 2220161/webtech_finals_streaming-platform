<?php
require('require_session.php');
?>

<!DOCTYPE html>
<html>
<head>
    <title>Welcome CM</title>
</head>
<body>
    <h2>Welcome, <?php echo $_SESSION['username']; ?>!</h2>
    <p>You are logged in as a <?php echo $_SESSION['role']; ?>.</p>
    <a href="logout.php">Logout</a>
</body>
</html>