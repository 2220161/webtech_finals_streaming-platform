<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>User Data</title>
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #f2f2f2;
        }

        .popup {
            display: none;
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            padding: 20px;
            background-color: #fff;
            border: 1px solid #ddd;
            z-index: 1000;
        }
    </style>
</head>
<body>
    <a href='logout.php'>Logout</a>
    <a href='admin_add_user.php'>Users</a>
    <a href='admin_update_pass.php'>Change Password Requests</a>
    <button onclick='showAddUserPopup()'>Add User</button>
    <table border='1'>
        <thead>
            <tr>
                <th>Username</th>
                <th>Full Name</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>

    <?php
    require('require_session.php');
    while ($row = $result->fetch_assoc()) {
        $fullName = $row['f_name'] . ' ' . $row['l_name'];

        echo "<tr>
                <td>{$row['username']}</td>
                <td>{$fullName}</td>
                <td>{$row['email']}</td>
                <td>
                    <button onclick='showPopup(\"{$row['username']}\")'>Delete</button>
                </td>
            </tr>";
    }
    ?>
        </tbody>
    </table>

    <div id="deletePopup" class="popup">
        <form id="deleteForm" action='delete.php' method='post'>
            <input type='hidden' id='deleteUsername' name='username' value=''>
            <input type='password' name='admin_password' placeholder='Enter Admin Password' required>
            <input type='submit' value='Delete'>
        </form>
        <button onclick='hidePopup()'>Cancel</button>
    </div>

        <div id="addUserPopup" class="popup">
            <form id="addUserForm" action='admin_add_user.php' method='post'>
                <label for='newUsername'>Username:</label>
                <input type='text' id='newUsername' name='newUsername' required>

                <label for='firstName'>First Name:</label>
                <input type='text' id='firstName' name='firstName' required>

                <label for='lastName'>Last Name:</label>
                <input type='text' id='lastName' name='lastName' required>

                <label for='newEmail'>Email:</label>
                <input type='email' id='newEmail' name='newEmail' required>

                <input type='hidden' id='newPassword' name='newPassword' value='contentmanager'>

                <input type='hidden' id='newRole' name='newRole' value='Content Manager'>

                <label for='adminPassword'>Admin Password:</label>
                <input type='password' id='adminPassword' name='adminPassword' placeholder='Enter Admin Password' required>
                <div id="adminPasswordError" style="color: red;"></div>

                <button onclick="showAddUserPopup()">Add User</button>
            </form>
            <button onclick='hideAddUserPopup()'>Cancel</button>
        </div>

        <script src="admin.js"></script>
    </body>
    </html>