<!DOCTYPE html>
<html lang="en">
<head>
    <title>Change Password</title>
    <script src="login.js" defer></script>
</head>
<body>

<h2>Change Password</h2>

<?php
require('require_session.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $new_password = $_POST['new_password'];
    $confirm_password = $_POST['confirm_password'];

    if ($new_password !== $confirm_password) {
        echo "<p style='color: red;'>The new and confirm passwords must match.</p>";
    } else {
        if (strlen($new_password) < 6) {
            echo "<p style='color: red;'>Password must be at least 6 characters long.</p>";
        } else {
            require('../database/db.php');

            $hashed_password = password_hash($new_password, PASSWORD_DEFAULT);
            $username = $_SESSION['username'];
            $sql = "UPDATE ACCOUNTS SET password = ? WHERE username = ?";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("ss", $hashed_password, $username);
            $stmt->execute();
            $stmt->close();
            $db->close();

            echo "<script>
                    alert('Password changed successfully');
                    window.location.href = 'cm.php';
                  </script>";
            exit();
        }
    }
}
?>

<form action="change_pass.php" method="post">
    <label for="new_password">New Password:</label>
    <input type="password" id="new_password" name="new_password" required>
    <label for="confirm_password">Confirm Password:</label>
    <input type="password" id="confirm_password" name="confirm_password" required>
    <input type="checkbox" onclick="toggleChangePassword()"> Show Password<br><br>
    <input type="submit" value="Change Password">
</form>

</body>
</html>
