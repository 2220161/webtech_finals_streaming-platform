<?php
require('../database/db.php');
require('require_session.php');

$sql = "SELECT username, f_name, l_name, email FROM accounts WHERE role = 'Content Manager'";
$result = $db->query($sql);

if (!$result) {
    die("Query failed: " . $db->error);
}

include('admin_html.php');
?>

<script src="admin.js"></script>

<?php
$result->free_result();
$db->close();
?>
