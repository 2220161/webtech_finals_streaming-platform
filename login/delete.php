<?php
include("../database/db.php");
require('require_session.php');

$delete_stmt = null;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST['username'];

    if (isset($_POST['admin_password'])) {
        $admin_password = $_POST['admin_password'];

        if (isset($_SESSION['username'])) {
            $admin_username = $_SESSION['username'];
            $admin_sql = "SELECT password FROM accounts WHERE username = ?";
            $admin_stmt = $db->prepare($admin_sql);
            $admin_stmt->bind_param("s", $admin_username);
            $admin_stmt->execute();
            $admin_result = $admin_stmt->get_result();

            if ($admin_result->num_rows > 0) {
                $admin_row = $admin_result->fetch_assoc();
                if (password_verify($admin_password, $admin_row['password'])) {

                    $delete_sql = "DELETE FROM accounts WHERE username = ?";
                    $delete_stmt = $db->prepare($delete_sql);
                    $delete_stmt->bind_param("s", $username);

                    if ($delete_stmt->execute()) {
                        header('Location: admin.php');
                    } else {
                        echo "Error deleting user: " . $delete_stmt->error;
                    }
                } else {
                    echo "Incorrect password";
                }
            }
        }
    } else {
        echo "Admin password not provided";
    }
}

if ($delete_stmt) {
    $delete_stmt->close();
}
$db->close();
?>
