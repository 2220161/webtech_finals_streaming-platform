function showPopup(username) {
    document.getElementById('deleteUsername').value = username;
    document.getElementById('deletePopup').style.display = 'block';
}

function hidePopup() {
    document.getElementById('deletePopup').style.display = 'none';
}

function showAddUserPopup() {
    var addUserPopup = document.getElementById('addUserPopup');
    addUserPopup.style.display = 'block';

}

function hideAddUserPopup() {
    var addUserPopup = document.getElementById('addUserPopup');
    addUserPopup.style.display = 'none';
}

function addUser() {
    var form = document.getElementById('addUserForm');
    var formData = new FormData(form);

    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'add_user.php', true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var response = JSON.parse(xhr.responseText);
            var errorMessageElement = document.getElementById('adminPasswordError');

            if (response.success) {
               hidePopup('addUserPopup');
            } else {
                errorMessageElement.innerText = response.message;
                document.getElementById('adminPassword').value = '';
            }
        }
    };
    xhr.send(formData);
}