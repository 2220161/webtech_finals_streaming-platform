<?php
require('../database/db.php');
session_start();

$identifier = $_POST['username'];
$password = $_POST['password'];

// Check if any user is already logged in
if (isset($_SESSION['logged_users']) && in_array($identifier, $_SESSION['logged_users'])) {
    $_SESSION['error'] = 'User already logged in';
    header('Location: index.php');
    exit();
}

$sql = "SELECT username, email, password, role FROM ACCOUNTS WHERE (username = ? OR email = ?)";
$stmt = $db->prepare($sql);
$stmt->bind_param("ss", $identifier, $identifier);

$stmt->execute();

$result = $stmt->get_result();

if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    if (password_verify($password, $row['password'])) {
        // Successful login
        $_SESSION['loggedin'] = true;
        $_SESSION['username'] = $row['username'];
        $_SESSION['role'] = $row['role'];

        // Add the logged-in user to the list
        $_SESSION['logged_users'][] = $row['username'];

        // Redirect based on role
        if ($_SESSION['role'] == 'Admin' || $_SESSION['role'] == 'admin') {
            header("location: admin.php");
            exit();
        } elseif ($_SESSION['role'] == 'Content Manager' || $_SESSION['role'] == 'content manager') {
            if ($password == 'content_manager') {
                header("location: change_pass.php");
            } else {
                header("location: cm.php");
            }
            exit();
        }
    } else {
        // Incorrect password
        $_SESSION['error'] = 'Invalid password';
        header('Location: index.php');
        exit();
    }
} else {
    // Account not found
    $_SESSION['error'] = 'Account not found';
    header('Location: index.php');
    exit();
}

$stmt->close();
$db->close();
?>
