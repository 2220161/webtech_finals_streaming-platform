<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Update Password</title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
    <a href='logout.php'>Logout</a>
    <a href='admin.php'>Users</a>
    <a href='admin_update_pass.php'>Change Password Requests</a>
<h2>Request for Password Reset</h2>

<table>
    <thead>
    <tr>
        <th>Username</th>
        <th>Name</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>

    <?php
    require('../database/db.php');
    session_start();

    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['reset'])) {
        $resetUsername = $_POST['reset'];
        $resetPassword = password_hash('content_manager', PASSWORD_DEFAULT);

        $updateSql = "UPDATE accounts SET password = '$resetPassword', forgot_pass_status = 0 WHERE username = '$resetUsername'";
        $db->query($updateSql);
    }

    $sql = "SELECT username, f_name, l_name FROM accounts WHERE forgot_pass_status = 1";
    $result = $db->query($sql);

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $fullName = $row['f_name'] . ' ' . $row['l_name'];
            echo "<tr>
                    <td>{$row['username']}</td>
                    <td>{$fullName}</td>
                    <td>
                        <form method='post'>
                            <input type='hidden' name='reset' value='{$row['username']}'>
                            <input type='submit' value='Reset'>
                        </form>
                    </td>
                  </tr>";
        }
    } else {
        echo "<tr><td colspan='3'>No Password Reset Request/s</td></tr>";
    }
    $db->close();
    ?>

    </tbody>
</table>

</body>
</html>